module Main exposing (..)

import Html exposing (..)


type alias Set =
    Int -> Bool



-- empty : Set
-- singleton : Int -> Set
-- interval : Int -> Int -> Set
-- union : Set -> Set -> Set
-- intersect : Set -> Set -> Set
-- toString : Set -> String


type alias RealFunc =
    Float -> Float



-- sumf : RealFunc -> RealFunc -> RealFunc
-- multf : RealFunc -> RealFunc -> RealFunc
-- compose : RealFunc -> RealFunc -> RealFunc
-- derive : RealFunc -> RealFunc
-- makeTable : RealFunc -> Float -> Float -> Float -> String


type alias Coder =
    Char -> Char



-- encode: String -> Coder -> String
-- isBijective : Coder -> Boolean


main =
    Html.text "Tukaj pretestirajte svoje funkcije"
