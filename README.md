# Druge vaje Elm@FRI

V drugem tednu se bomo spoznali s funkcijami, ki so v Elmu državljani prve sorte. Kar pomeni, da z njimi lahko delamo kot z navadnimi podatki in si jih podajamo okoli preko argumentov funkcij.

## Množice v Elmu

V prvi nalogi boste spoznali, kako lahko funkcije predstavljajo podatkovno strukturo množica.
```elm
type alias Set =
    Int -> Bool
```
Definirali smo torej tip množica, ki pa je v bistvu funkcija. Ta za podano celo število vrne ali pripada množici ali ne.

Najprej implementirajte tri funkcije za gradnjo osnovnih množic
```elm
empty : Set
singleton : Int -> Set
interval : Int -> Int -> Set
```
Prva funkcija preprosto vrne prazno množico (kaj je to?).
Druga funkcija za podano celo število vrne množico, ki vsebuje zgolj ta element. Tretja pa za podani celi števili zgradi množico, ki vsebuje vsa števila iz intervala (vključno s podanimi števili).

Nato implementirajte unijo in presek funkcij:
```elm
union : Set -> Set -> Set
intersect : Set -> Set -> Set
```
Nazadnje implementirajte še funkcijo:
```elm
show : Int -> Int -> Set -> String
```
ki sprejme dve števili (ki predstavljata interval) in množico, vrne pa predstavitev množice na podanem intervalu, tj. vse elemente v tem intervalu, ki so elementi podane množice.

Izhod funkcije naj bo nekaj takega:
```
{3, 5, 7, 8, 9}
```


## Realne funkcije
V tej nalogi pa si bomo ogledali delo s funkcijami relne spremenljivke. Najprej definirajmo tip take fukcije.

```elm
type alias RealFunc =
    Float -> Float
```
Nato implementirajte 4 funkcije, ki iz podanih funkcij konstruirajo nove realne funkcije:

```elm
sumf : RealFunc -> RealFunc -> RealFunc
multf : RealFunc -> RealFunc -> RealFunc
compose : RealFunc -> RealFunc -> RealFunc
derive : RealFunc -> RealFunc
```
Prva funkcija naj vrne novo fukcijo, ki predstavlja vsoto dveh podanih realnih funkcij, druga produkt, tretja pa kompozitum. Četrta funkcija naj vrne novo funkcijo, ki predstavlja odvod podane funkcije (implementirajte odvajanje enostavno numerično).

Nazadnje, da boste lahko preverjali delovanje, pa implementirajte še tabeliranje podane funkcije.
```elm
makeTable : RealFunc -> Float -> Float -> Float -> String
```
Ta funkcija sprejme 1) realno funkcijo, 2) začetek intervala tabeliranja, 3) konec intervala tabeliranja in 4) korak tabeliranja. Vrne pa tabelo kot niz.

```elm
f: RealFunc
f x = x*x+3
makeTable f 0.0 1.0 0.1
```
Izhod `makeTable` naj bo niz, npr. nekaj podobno:
```
   x    |       f(x)
---------------------------
0       |      3.0
0.1     |      3.01
0.2     |      3.04
0.3     |      3.09
0.4     |      3.16
0.5     |      3.25
0.6     |      3.36
0.7     |      3.49
0.8     |      3.64
0.9     |      3.91
1.0     |      4.0
```

## Šifriranje nizov :crown:
Spodnjo nalogo vzemite kot izziv, kjer boste morali malo pobrskati po standardni knjižnici za delo z nizi in znaki. Predpostavite, da so znaki iz nabora ASCII, tj. vrednosti med 0 in 255.


Vsako funkcijo `Char -> Char` si lahko predstavljamo kot šifrirno funkcijo.

```elm
type alias Coder =
    Char -> Char
```

 Napišite nekaj funkcij za delo s takimi šifrirnimi funkcijami.
 Najprej napišite funkcijo, ki uporabi poljubni kodirnik in zakodira podani niz (vrnjeni niz je zakodirani podani niz).
 ```elm
encode : String -> Coder -> String
 ```
Če želimo, da je kodirna funkcija uporabna, potem mora biti bijektivna. To namreč pomeni, da bo mogoče zakodirani niz sploh enolično odkodirati.
Napišite funkcijo, ki bo preverila, če je podani kodirnik bijektivna funkcija.
```elm
isBijective : Coder -> Bool
```

Za odkodiranje podanega niza potrebujete inverzno funkcijo (ki pa je tudi kodirnik). Zapišite funkcijo, ki za podani kodirnik vrne njegov inverz.
```elm
inverse: Coder -> Coder
```
